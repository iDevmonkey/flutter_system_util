#import "FlutterSystemUtilPlugin.h"

@import AVFoundation;

@implementation FlutterSystemUtilPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
      methodChannelWithName:@"flutter_system_util"
            binaryMessenger:[registrar messenger]];
  FlutterSystemUtilPlugin* instance = [[FlutterSystemUtilPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"vibrator" isEqualToString:call.method]) {
      [self vibrator:result];
  }
  else {
    result(FlutterMethodNotImplemented);
  }
}

- (void)vibrator:(FlutterResult)result {
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    result(@YES);
}

@end
