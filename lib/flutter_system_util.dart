import 'dart:async';

import 'package:flutter/services.dart';

class FlutterSystemUtil {
  static const MethodChannel _channel =
      const MethodChannel('flutter_system_util');

  static Future vibrator() async {
    await _channel.invokeMethod('vibrator');
  }
}
