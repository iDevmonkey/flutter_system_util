package hzh.flutter_system_util;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import android.content.Context;
import android.os.Vibrator;

/** FlutterSystemUtilPlugin */
public class FlutterSystemUtilPlugin implements MethodCallHandler {
  private final Context context;

  private FlutterSystemUtilPlugin(Registrar registrar) {
    this.context = registrar.context();
  }

  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_system_util");
    channel.setMethodCallHandler(new FlutterSystemUtilPlugin(registrar));
  }

  @Override
  public void onMethodCall(MethodCall call, Result result) {
    if (call.method.equals("vibrator")) {
      vibrator(result);
    } else {
      result.notImplemented();
    }
  }

  private void vibrator(Result result) {
    Vibrator vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
    vibrator.vibrate(200);
    result.success(true);
  }
}
